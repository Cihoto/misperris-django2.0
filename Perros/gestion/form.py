from django import forms
from .models import Persona

class PersonaForm(forms.ModelForm):
    class Meta:
        model=Persona
        fields=('nombre','rut','cel','fec_nac','correo')#parametros que queremos que aparescan

#class AlumnoForm(form.ModelForm):
#    class Meta:
#        model=Alumno
#        fields = ('','','')

#Python se ajusta cada tipo de dato nosotros solo definimos cuales solon los que
#queremos que aparescan

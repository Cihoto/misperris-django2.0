# Generated by Django 2.1.2 on 2018-10-30 20:06

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gestion', '0003_auto_20181030_1659'),
    ]

    operations = [
        migrations.AlterField(
            model_name='persona',
            name='cel',
            field=models.IntegerField(validators=[django.core.validators.MaxValueValidator(999999999)], verbose_name='Número de celular'),
        ),
    ]

from django.db import models
from django.core.validators import MaxValueValidator,MinValueValidator

# Create your models here.
class Persona(models.Model):
    nombre = models.CharField(max_length=50)
    rut = models.CharField(max_length=10)
    cel = models.IntegerField('Número de celular',validators=[MaxValueValidator(999999999),MinValueValidator(900000000)])
    fec_nac = models.DateField('Año de nacimiento')
    correo = models.EmailField('Ingrese su correo')
    def __str__(self):
        
        return '{} {} {} {} {}'.format(self.nombre,self.rut,str(self.cel),str(self.fec_nac),str(self.correo))






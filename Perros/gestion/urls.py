from django.conf.urls import url
from . import views 

urlpatterns = [
    url(r'^$',views.index),
    url(r'^Persona/listar/$', views.per_list),
    url(r'^Persona/detalle/(?P<pk>[0-9]+)/$',views.persona_detalle,''),
    url(r'^Persona/eliminar/(?P<pk>[0-9]+)/$',views.persona_eliminar,''),
    url(r'^Persona/editar/(?P<pk>[0-9]+)/$',views.persona_editar),
    url(r'^Persona/nuevo/$',views.persona_nueva),
]
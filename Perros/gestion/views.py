from django.shortcuts import render, get_object_or_404
import time 
from django import forms
from .models import Persona
from .form import PersonaForm



# Create your views here.

def index(request):
    a = time.strftime("%c")
    return render(request, 'base.html',{'fechaHora':a})



def per_list(request):
    per= Persona.objects.all()
    return render(request, 'listar_personas.html',{'Persona':per})

def persona_detalle(request, pk):
    per= get_object_or_404(Persona,pk=pk)
    return render(request, 'detalle_persona.html',{'Persona':per})

def persona_eliminar(request, pk):
    Persona.objects.filter(pk=pk).delete()
    per= Persona.objects.all()
    return render(request, 'listar_personas.html',{'Persona':per})

def persona_editar(request, pk):
    per= get_object_or_404(Persona,pk=pk)
    if request.method=="POST":
        form = PersonaForm(request.POST, instance=per)

        if form.is_valid():
            per = form.save(commit = False)
            per.save()
            return render(request, 'detalle_persona.html' , {'Persona':per})
    else:
        form=PersonaForm(instance=per)
        return render(request, 'editar_persona.html' , {'form':form})


def persona_nueva(request):
    if request.method=="POST":
        form=PersonaForm(request.POST)
        if form.is_valid():
            per=form.save(commit=False)
            per.save()
            return render(request, 'detalle_persona.html',{'Persona':per})
    else:
        form = PersonaForm()
        return render(request,'editar_persona.html',{'form':form})
     
    
    


